# PICA-Daten zu Druckwerken des Verlags Gebauer

## Datenquellen

### CERL – Heritage of the Printed Book Database

> The HPB Database (previously called the Hand Press Book Database) is a steadily growing collection of files of catalogue records from major European and North American research libraries covering items of European printing of the hand-press period (c.1455-c.1830) integrated into one file. ... The HPB Database is of interest to librarians and any one else with academic pursuits across many fields of study that use printed books as source material. It is especially valuable for research in intellectual history, social history, and transmission of thought – as well as in the history of printing and the history of the book. ([Quelle](https://gso.gbv.de/DB=1.77/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/hpb
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.pbl=Gebauer](http://sru.gbv.de/hpb?version=2.0&operation=searchRetrieve&query=pica.pbl%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.pub=Gebauer](http://sru.gbv.de/hpb?version=2.0&operation=searchRetrieve&query=pica.pub%3DGebauer&recordSchema=picaxml&maximumRecords=5)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/hpb) vom GBV.

### Gemeinsamer Verbundkatalog ([DE-627](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627))

> Der gemeinsame Verbundkatalog (GVK) ist der frei zugängliche Ausschnitt der Verbunddatenbank K10plus mit den Nachweisen der GBV-Bibliotheken und der SWB-Bibliotheken. ... Im gemeinsamen Verbundkatalog (GVK) sind über 65 Mio. Titel von Büchern, Zeitschriften, Aufsätzen, Kongressberichten, Mikroformen, elektronischen Dokumenten, Datenträgern, Musikalien, Karten etc. von über 1.000 Bibliotheken aus dem GBV und dem SWB nachgewiesen. ([Quelle](https://kxp.k10plus.de/DB=2.1/))

_Search and Retrieve via URL_

- Dokumentation: https://wiki.k10plus.de/display/K10PLUS/SRU
- Endpunkt: http://sru.k10plus.de/gvk
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.vlg=Gebauer](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.vlg%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.dru=Gebauer](http://sru.k10plus.de/gvk?version=2.0&operation=searchRetrieve&query=pica.dru%3DGebauer&recordSchema=picaxml&maximumRecords=5)
- Datenlizenz: [CC0 1.0](https://www.gbv.de/Verbundzentrale/benutzungs-und-entgeltordnung-der-verbundzentrale)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](https://uri.gbv.de/database/gvk) vom GBV.

### VD 18 – Das Verzeichnis Deutscher Drucke des 18. Jahrhunderts ([DE-627-5](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627-5))

> In diesem "Verzeichnis der im deutschen Sprachraum erschienenen Drucke des 18. Jahrhunderts" werden alle zwischen 1701 und 1800 in deutscher Sprache oder im deutschen Sprachraum erschienenen Drucke kooperativ erfasst und mit einer individuellen VD18-Nummer versehen. Derzeit (Stand: Februar 2018) enthält die von der Verbundzentrale des Gemeinsamen Bibliotheksverbunds betreute VD18-Datenbank rund 178.000 Monographien, 9.500 mehrbändige Werke mit 28.000 Bänden und ca. 3.300 Zeitschriftentitel. ([Quelle](https://gso.gbv.de/DB=1.65/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/vd18
    - jetzt auch über den K10plus: http://sru.k10plus.de/vd18
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.pub=Gebauer](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.pub%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.dru=Gebauer](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.dru%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.drx=Gebauer](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.drx%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.dru=Gebauer](http://sru.k10plus.de/vd18?version=2.0&operation=searchRetrieve&query=pica.dru%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.vlg=Gebauer](http://sru.gbv.de/vd18?version=2.0&operation=searchRetrieve&query=pica.vlg%3DGebauer&recordSchema=picaxml&maximumRecords=5),

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/vd18) vom GBV.

### Gelehrte Journale und Zeitungen der Aufklärung ([DE-627](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-627))

> Seit dem Jahr 2011 werden im Rahmen des Projektes "Gelehrte Journale und Zeitungen als Netzwerke des Wissens im Zeitalter der Aufklärung" die bedeutendsten deutschsprachigen Vertreter der fächerübergreifenden polyhistorischen Zeitschriften bibliographisch und inhaltlich erschlossen sowie in digitalisierter Form verfügbar gemacht. Unter der Trägerschaft der Akademie der Wissenschaften zu Göttingen, mit Arbeitsstellen in Göttingen, Leipzig und München und in Kooperation mit der Niedersächsischen Staats- und Universitätsbibliothek Göttingen, der Universitätsbibliothek Leipzig und der Bayerischen Staatsbibliothek München wird mit dieser Datenbank bis zum Jahr 2025 ein Quellenfundus aus 323 Zeitschriften (ca. 2.775 Bände, ca. 1.260.000 Seiten) entstehen. Der Erschließungszeitraum reicht von 1688 bis 1815 und bildet damit das gesamte Spektrum der Gelehrten Journale und Zeitungen der Aufklärung ab. ([Quelle](https://gelehrte-journale.de/ueber-uns/projekte-und-datenbanken/))

_Search and Retrieve via URL_

- Dokumentation: https://verbundwiki.gbv.de/display/VZG/SRU
- Endpunkt: http://sru.gbv.de/gjz18
- Format: [picaxml](http://format.gbv.de/pica/xml)
- Abfragen: [pica.vlg=Gebauer](http://sru.gbv.de/gjz18?version=2.0&operation=searchRetrieve&query=pica.vlg%3DGebauer&recordSchema=picaxml&maximumRecords=5), [pica.pub=Gebauer](http://sru.gbv.de/gjz18?version=2.0&operation=searchRetrieve&query=pica.pub%3DGebauer&recordSchema=picaxml&maximumRecords=5)
- Datenlizenz: [CC BY-SA 4.0](https://gelehrte-journale.de/impressum/)

Siehe auch den entsprechenden [Eintrag im Datenbankverzeichnis](http://uri.gbv.de/database/gjz18) vom GBV.

### Zeitschriftendatenbank ([DE-600](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-600))

> Die Zeitschriftendatenbank (ZDB) ist eine der weltweit größten Datenbanken für den Nachweis von Zeitschriften und Zeitungen aus allen Ländern, in allen Sprachen, ohne zeitliche Einschränkung, in gedruckter, elektronischer oder anderer Form. ... Aktuell bringen 3.700 Bibliotheken aus Deutschland und Österreich ihre Zeitschriftentitel und die zugehörigen Bestandsnachweise in die ZDB ein. ... Ingesamt umfasst die ZDB mehr als 1,8 Mio. Titel- und 16,6 Mio. Bestandsnachweise. ([Quelle](https://sigel.staatsbibliothek-berlin.de/suche/?isil=DE-600))

_Search and Retrieve via URL_

- Dokumentation: https://www.zeitschriftendatenbank.de/services/schnittstellen/sru/
- Endpunkt: http://services.dnb.de/sru/zdb
- Format: [PicaPlus-xml](http://format.gbv.de/pica/ppxml)
- Abfrage: [dnb.eje>=1734 and dnb.eje<=1819 and dnb.ver=Gebauer](http://services.dnb.de/sru/zdb?version=1.1&operation=searchRetrieve&query=dnb.eje%3E%3D1734%20and%20dnb.eje%3C%3D1819%20and%20dnb.ver%3DGebauer&recordSchema=PicaPlus-xml&maximumRecords=5)
- Datenlizenz: [CC0 1.0](https://www.zeitschriftendatenbank.de/de/ueber-uns/datenlizenz/)

## Verwendete Software

- [Perl](https://www.perl.org/)
    - [Catmandu](https://metacpan.org/release/Catmandu) ([Handbook](https://librecat.org/Catmandu/))
        - [Catmandu::PICA](https://metacpan.org/release/Catmandu-PICA)
        - [Catmandu::SRU](https://metacpan.org/release/Catmandu-SRU)
