# to be run from project dir ../

library(picar)

PICA_DRU <- "data/vd18/pica-dru-gebauer/records.json"

# get pica items with "Gebauer" as printer
items <- get_data(PICA_DRU)

# exclude authority files (persons, corporates)
items <- gbv_filter_bibtype(items, "A|O")

# collect data from items
identifiers <- sapply(items, vd18_number)
titles <- sapply(items, gbv_maintitle)
titles[sapply(titles, function(x) identical(x, character(0)))] <- ""
publishers <- sapply(items, gbv_publisher, collapse=TRUE)
publishers[sapply(publishers, function(x) identical(x, NULL))] <- ""
publishers <- gsub("^ | $","", publishers)
places <- sapply(items, gbv_place, collapse=TRUE)
places[sapply(places, function(x) identical(x, NULL))] <- ""
printers <- sapply(items, adr_printer, collapse=TRUE)
printers[sapply(printers, function(x) identical(x, NULL))] <- ""
printers <- gsub("^ | $","", printers)
# printers <- gsub("|","", printers, fixed=T)
printers_gnd <- sapply(items, adr_printer_gnd, collapse=TRUE)
printers_gnd[sapply(printers_gnd, function(x) identical(x, NULL))] <- ""
printers_gnd <- gsub("gnd/", "", printers_gnd, fixed=T)
dates <- sapply(items, gbv_year)
dates[sapply(dates, function(x) identical(x, NULL))] <- ""
types <- sapply(items, gbv_bibtype)

# create data frame
df <- data.frame(ppn=names(items), vd18_nr=as.character(identifiers), type=as.character(types),
                 printer=as.character(printers), printer_gnd=as.character(printers_gnd),
                 publisher=as.character(publishers), place=as.character(places),
                 date=as.character(dates), title=as.character(titles), stringsAsFactors=FALSE)
df <- df[order(df$date, df$type),]

# save as csv file
write.csv(df, file="view/output/vd18-dru--any--all.csv", row.names=F)

# save item subsets as csv files
printed_any <- df[grepl("A", df$type),]
write.csv(printed_any, file="view/output/vd18-dru--any--printed.csv", row.names=F)
repro_any <- df[grepl("O", df$type),]
write.csv(repro_any, file="view/output/vd18-dru--any--online.csv", row.names=F)
printed_mono <- df[grepl("Aa", df$type),]
write.csv(printed_mono, file="view/output/vd18-dru--mono--printed.csv", row.names=F)
repro_mono <- df[grepl("Oa", df$type),]
write.csv(repro_mono, file="view/output/vd18-dru--mono--online.csv", row.names=F)
printed_multi <- df[grepl("Ac", df$type),]
write.csv(printed_multi, file="view/output/vd18-dru--multi--printed.csv", row.names=F)
repro_multi <- df[grepl("Oc", df$type),]
write.csv(repro_multi, file="view/output/vd18-dru--multi--online.csv", row.names=F)
printed_part <- df[grepl("Af", df$type),]
write.csv(printed_part, file="view/output/vd18-dru--part--printed.csv", row.names=F)
repro_part <- df[grepl("Of", df$type),]
write.csv(repro_part, file="view/output/vd18-dru--part--online.csv", row.names=F)
